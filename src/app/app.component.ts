import { Component } from '@angular/core';

import { AuthService } from './login/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'App Works';

  mostrarMenu:false;

  constructor(private authService:AuthService){}

  ngOnInit(): void {
    this.authService.showMenuEmiiter
      .subscribe(show=>{
        this.mostrarMenu = show;
      });
  }
}
