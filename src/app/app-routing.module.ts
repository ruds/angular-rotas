import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./login/login.component";

const APP_ROUTES: Routes = [
  {path:'', component: HomeComponent},
  {path:'home', component: HomeComponent},
  {path:'login', component: LoginComponent},
]; 


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(APP_ROUTES),
  ],
  declarations: [],
  exports:[RouterModule]
})
export class AppRoutingModule { }
