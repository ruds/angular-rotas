import { NgModule } from '@angular/core';
import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { CursosComponent } from "./cursos.component";
import { CursoIDComponent } from "./curso-id/curso-id.component";
import { CursoNotFoundComponent } from "./curso-not-found/curso-not-found.component";

const CURSOS_ROUTES: Routes = [
  {path:'cursos', component: CursosComponent},
  {path:'curso/:id', component: CursoIDComponent},
  {path:'cursonotfound', component: CursoNotFoundComponent},
]; 

@NgModule({
  imports: [
    RouterModule.forChild(CURSOS_ROUTES)
  ],
  exports: [RouterModule]
})
export class CursosRoutingModule { }
