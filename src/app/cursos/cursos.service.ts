import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CursosService {

  constructor() { }

  getCursos(){
    return [
      {id:0, name: 'Java'},
      {id:1, name: 'Node'},
      {id:2, name: 'PHP'},
    ];
  }

  getCurso(id:number):any{
    for (const curso of this.getCursos()) {
      if (curso.id == id)
        return curso;
    }
    return null;
  }
}
