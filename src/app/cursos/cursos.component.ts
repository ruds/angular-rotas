import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { CursosService } from './cursos.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css']
})
export class CursosComponent implements OnInit {

  cursos:Array<any> = [];
  pagina:number = 0;
  subscribe: Subscription;

  constructor(
    private cursoService:CursosService,
    private route:ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.cursos = this.cursoService.getCursos();
    this.subscribe = this.route.queryParams.subscribe((queryParams)=>{
      this.pagina = queryParams['pagina'];
    });
  }

  ngOnDestroy(): void {
    this.subscribe.unsubscribe();
  }

  nextPage(){
    this.router.navigate(['/cursos'],{queryParams:{'pagina': ++this.pagina}});
  }

}