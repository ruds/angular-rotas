import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { CursosService } from '../cursos.service';

@Component({
  selector: 'app-curso-id',
  templateUrl: './curso-id.component.html',
  styleUrls: ['./curso-id.component.css']
})
export class CursoIDComponent implements OnInit {

  id:number;
  subscribe: Subscription
  curso:any;

  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private cursoService:CursosService
  ) { }

  ngOnInit() {
    this.subscribe = this.route.params.subscribe((params:any)=>{
      this.id = params['id'];
    });
    this.curso = this.cursoService.getCurso(this.id);

    if(this.curso==null){
      this.router.navigate(['/cursonotfound']);
    }

  }

  ngOnDestroy(): void {
    this.subscribe.unsubscribe();
  }

}