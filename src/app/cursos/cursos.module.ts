import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CursosRoutingModule } from './cursos-routing.module';
import { CursosComponent } from './cursos.component';
import { CursoIDComponent } from './curso-id/curso-id.component';
import { CursoNotFoundComponent } from './curso-not-found/curso-not-found.component';
import { CursosService } from './cursos.service';

@NgModule({
  imports: [
    CommonModule,
    CursosRoutingModule,
  ],
  declarations: [
    CursosComponent,
    CursoIDComponent,
    CursoNotFoundComponent
  ],
  providers:[CursosService],
  exports:[CursosRoutingModule]
})
export class CursosModule { }
