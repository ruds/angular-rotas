import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlunosComponent } from './alunos.component';
import { AlunoFormComponent } from './aluno-form/aluno-form.component';
import { AlunoInfoComponent } from './aluno-info/aluno-info.component';


const ALUNOS_ROUTES: Routes = [
  {path:'alunos', component: AlunosComponent, children:[
    {path:'novo', component:AlunoFormComponent},
    {path:':id', component:AlunoInfoComponent},
    {path:':id/editar', component:AlunoFormComponent},
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(ALUNOS_ROUTES)],
  exports: [RouterModule]
})
export class AlunosRoutingModule { }
