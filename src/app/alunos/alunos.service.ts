import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlunosService {

  private alunos:Array<any> = [
    {id: 0, name: 'Jhon', email:'jhon@mail.com'},
    {id: 1, name: 'Mary', email:'mary@mail.com'},
    {id: 2, name: 'Brian', email:'brian@mail.com'},
  ];

  constructor() { }

  getAlunos():Array<any>{
    return this.alunos;
  }

  getAluno(id:number):any{
    for (const aluno of this.getAlunos()) {
      if (aluno.id == id)
        return aluno;
    }
    return null;
  }
}
