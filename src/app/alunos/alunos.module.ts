import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AlunosRoutingModule } from './alunos-routing.module';
import { AlunosComponent } from './alunos.component';
import { AlunoFormComponent } from './aluno-form/aluno-form.component';
import { AlunoInfoComponent } from './aluno-info/aluno-info.component';
import { AlunosService } from './alunos.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AlunosRoutingModule,
  ],
  declarations: [AlunosComponent, AlunoFormComponent, AlunoInfoComponent],
  providers:[AlunosService]
})
export class AlunosModule { }
