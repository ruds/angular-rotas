import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { AlunosService } from '../alunos.service';

@Component({
  selector: 'app-aluno-form',
  templateUrl: './aluno-form.component.html',
  styleUrls: ['./aluno-form.component.css']
})
export class AlunoFormComponent implements OnInit, OnDestroy {

  id:number;
  subscribe: Subscription
  aluno:any;

  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private alunosService:AlunosService
  ) { }

  ngOnInit() {

     this.subscribe = this.route.params.subscribe((params:any)=>{
      this.id = params['id'];
    });
    this.aluno = this.alunosService.getAluno(this.id);

    if(this.aluno === null)
      this.aluno = {};
  }

  ngOnDestroy(): void {
    this.subscribe.unsubscribe();
  }

}
