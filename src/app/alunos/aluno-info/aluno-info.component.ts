import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { AlunosService } from '../alunos.service';

@Component({
  selector: 'app-aluno-info',
  templateUrl: './aluno-info.component.html',
  styleUrls: ['./aluno-info.component.css']
})
export class AlunoInfoComponent implements OnInit, OnDestroy {

  id:number;
  subscribe: Subscription
  aluno:any;

  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private alunosService:AlunosService
  ) { }

  ngOnInit() {

     this.subscribe = this.route.params.subscribe((params:any)=>{
      this.id = params['id'];
    });
    this.aluno = this.alunosService.getAluno(this.id);
  }

  ngOnDestroy(): void {
    this.subscribe.unsubscribe();
  }

  editarContato(){
    this.router.navigate(['/alunos', this.aluno.id, 'editar']);
  }

}