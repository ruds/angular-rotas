import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./login/login.component";
import { CursosComponent } from "./cursos/cursos.component";
import { CursoIDComponent } from "./cursos/curso-id/curso-id.component";
import { CursoNotFoundComponent } from "./cursos/curso-not-found/curso-not-found.component";

const APP_ROUTES: Routes = [
  {path:'', component: HomeComponent},
  {path:'home', component: HomeComponent},
  {path:'login', component: LoginComponent},
  {path:'cursos', component: CursosComponent},
  {path:'curso/:id', component: CursoIDComponent},
  {path:'cursonotfound', component: CursoNotFoundComponent},

]; 

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);