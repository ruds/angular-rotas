import { Injectable, EventEmitter } from '@angular/core';
import { Usuario } from './usuario';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private usuarioAuth:boolean = false;
  public showMenuEmiiter = new EventEmitter<boolean>();
 
  constructor(private router:Router) { }

  fazerLogin(usuario:Usuario){
    if(usuario.nome==='ruds' && usuario.senha==='1234'){
      this.usuarioAuth = true;
      this.showMenuEmiiter.emit(true);
      this.router.navigate(['/']);
    }
    else {
      this.showMenuEmiiter.emit(false);
      this.usuarioAuth = false;
    }
  }
}
